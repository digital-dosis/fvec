
var langs = ['es', 'ca'];
var langCode = '';
var langJS = null;


var translate = function (jsdata){	
	$("[tkey]").each (function (index){
		var strTr = jsdata [$(this).attr ('tkey')];
	    $(this).html (strTr);
	});
}

$(document).ready(function(){


	langCode = navigator.language.substr (0, 2);
	checkLang();

	$('.translate').on('click', function(e){
		e.preventDefault();
		langCode = $(this).attr('id');
		checkLang();
	});


});


function checkLang(){
	if(jQuery.inArray( langCode,langs) == -1){
		$.getJSON('languages/es.json', translate);
	}else{
		$.getJSON('languages/'+langCode+'.json', translate);
	}
}