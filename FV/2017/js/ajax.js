var isAnimating = false;
var isBackwards = false;
var coverScrolled = false;
var isMobile = false;
var isLandinglink = false;
var page;
var newPage;
var currentPage;
var language = "";
var sections = { 
	index : {
		animationTimeEnter: 1000,
		animationTimeExit: 1000,
		animationFunctionEnter: function() { homeAnimationEnter(); },
		animationFunctionExit: function() { homeAnimationExit(); },
		animationFunctionEnterFromBack: function() {  },
		animationFunctionExitToBack: function() { }
	},
	hub : {
		animationTimeEnter: 1000,
		animationTimeExit: 1000,
		animationFunctionEnter: function() { hubAnimationEnter(); },
		animationFunctionExit: function() { hubAnimationExit(); },
		animationFunctionEnterFromBack: function() { hubAnimationEnterFromBack(); },
		animationFunctionExitToBack: function() { hubAnimationExitToBack(); }
	},
	landing: {
		animationTimeEnter: 1000,
		animationTimeExit: 2600,
		animationFunctionEnter: function() { landingAnimationEnter() },
		animationFunctionExit: function() { landingAnimationExit() } ,
		animationFunctionEnterFromBack: function() {  },
		animationFunctionExitToBack: function() { }
	}
};

var landingArray = ['no-pasa-nada', 'eres-impulso', 'muevete', 'kids', 'nuestra-gama'];
var currentLandingPosition = 0;
var currentLanding;


$(document).ready(function() {

	detectMobil();
	$( window ).resize(function() {
	  detectMobil();
	  $('.backimage').css('opacity', '0.2');
	});

	var pageArray = location.pathname.split('/');
	currentPage = pageArray[pageArray.length - 1].replace('.html', '');

	currentPage = checkIsLandingExtension(currentPage);


	checkLanguage();
	window.location.hash = '#' + language;

	$('body').on('click', '[data-type="page-transition"]', function(event){
	    event.preventDefault();

		isBackwards = false;

	    //detect which page has been selected
	    newPage = $(this).attr('href');
	    //if the page is not animating - trigger animation
	    if( !isAnimating ) changePage(newPage);

	    //detect if is landing link
	    if( $(this).hasClass('landing-transition') ){
	    	isLandinglink = true;
	    	console.log('islandinglink');
	    }else{
	    	isLandinglink = false;
	    	console.log('nonlandinglink');
	    }
	});


	window.addEventListener('popstate', function(event) {
		var newPageArray = location.pathname.split('/'),
	        newPage = newPageArray[newPageArray.length - 1];

		isBackwards = true;

		changePage(newPage);
	});

	//toggle menu
	$('.hamburger').on('click', function(){
		$(this).toggleClass('is-active');
		$('.main-menu-container').toggleClass('show');
		$('.content').toggleClass('menu-visible');
	});

	initSwiperMenu();

	//landing cover block
	$('body').on('click','.arrow-down', function(){
			coverScrolled = true;
			scrollCover();
	});

	//desplegar info catalogo nuestra-gama
	$('body').on('click','.bottle', function(){
		var selectedBtl = $(this).attr("class");
		var classSelectedBtl = selectedBtl.replace('bottle ', '');

		//set arrow-position
		$(this).parent().next('.panel-info-bottle').find('#line-arrow').removeClass().addClass(classSelectedBtl);


		var infoBtl = '.label.'+classSelectedBtl;
		var panelSel = $(this).parent().next('.panel-info-bottle');


		if( panelSel.hasClass('open-panel') ){
			$('.label').removeClass('active-info');
			panelSel.find(infoBtl).addClass('active-info');
			$('.label h4, .label p, .label img').animate({opacity:0}, 100);
			$('.active-info h4, .active-info p, .active-info img').animate({opacity:1}, 800);
		}else{
			$('.panel-info-bottle').slideUp( "fast", function() {
			    $('.label').removeClass('active-info');
			    $('.panel-info-bottle').removeClass('open-panel');
			});
			panelSel.slideDown( "fast", function() {
				$(this).addClass('open-panel');
			    $(this).find(infoBtl).addClass('active-info');
			    $('.label h4, .label p, .label img').animate({opacity:0}, 100);
			    $('.active-info h4, .active-info p, .active-info img').animate({opacity:1}, 800);
		  	});
		}
	});


	//Social line rollover animation
	$('body').on('mouseenter','.social-item', function(){
		$('.social').addClass('tiny');
	}).on('mouseleave','.social-item', function(){
		$('.social').removeClass('tiny');
	});

	//Video link: line animation 
	$('body').on('mouseenter', '.videolink', function(){
		$('.line_composition, .videolink').addClass('animation');
	}).on('mouseleave', '.videolink', function(){
		$('.line_composition, .videolink').removeClass('animation');
	});

	//Hub controls: line animation 
	$('body').on('mouseenter','.hub-pagination', function(){
		$('.swiper-pagination-bullet-active').addClass('tiny');
	}).on('mouseleave','.hub-pagination', function(){
		$('.swiper-pagination-bullet-active').removeClass('tiny');
	});
	

	//Landing cover: arrow animation 
	$('body').on('mouseenter','.cover p', function(){
		$('.arrow-down').addClass('animation');
	}).on('mouseleave','.cover p', function(){
		$('.arrow-down').removeClass('animation');
	});



	//Detect click event icon-scroll home
	$('body').on('click','.icon-scroll', function() {
        isBackwards = false;

	    newPage = 'hub.html';
	    if( !isAnimating ) changePage(newPage);
	});


});

var translate = function (jsdata){	
	$("[tkey]").each (function (index){
		var strTr = jsdata [$(this).attr ('tkey')];
	    $(this).html (strTr);
	});
}

var langs = ['es', 'ca'];
var prevLanguage = language;
function checkLanguage() {
	prevLanguage = language;
	if (!$.isEmptyObject(window.location.hash)) {
		language = window.location.hash.substr(1,2);
	}

	if (!language) language = navigator.language.substr(0, 2);
	if($.inArray( language,langs) == -1) language = 'es';

	if (language != prevLanguage) {
		$.getJSON('languages/' + language + '.json', translate)
			.fail( function(d, textStatus, error) {
				console.error("getJSON failed, status: " + textStatus + ", error: "+error)
			});
		console.log("Json loaded");
	}

	console.log("Language:", language);
	$('.lang-selector a').removeClass('sel');
	$('#' + language).addClass('sel');
}


function changePage(url) {
	var newSectionName = url.replace('.html', '');

	currentPage === "" ? currentPage = 'index' : currentPage;
	newSectionName === "" ? newSectionName = 'index' : newSectionName;

	newSectionName = checkIsLandingExtension(newSectionName);

    isAnimating = true;

    if( isBackwards == true && newSectionName == 'index' ){
		sections[currentPage].animationFunctionExitToBack();
    }else{
    	sections[currentPage].animationFunctionExit();
    }

	setTimeout(function() {
		currentPage = newSectionName;
		loadNewContent(url);

	}, sections[currentPage].animationTimeExit);

}

function loadNewContent(url){
  	var section = $('<div class="ajax"></div>');

  	section.load( url + ' .content', function(event){

		$(section).find('.layer-mask').addClass('SlideInUp');
		if( !isMobile && ( isBackwards  ||  isLandinglink ) ){
			$(section).find('[data-index="' + currentLandingPosition + '"] .layer').addClass('covered');
			$(section).find('[data-index="' + currentLandingPosition + '"] .container_composition').removeClass('covered');
		}
      	
		$('#main').html(section);
		checkLanguage();
		if( isBackwards == true && currentPage == 'hub' ||  currentPage == 'hub' && isLandinglink ) {
			sections[currentPage].animationFunctionEnterFromBack();
			isLandinglink = false;
			$('.brand').removeClass('landing-transition');
			console.log('click in landing link-hub');
		}
		$('.main-menu-container').removeClass('show');
		$('.hamburger').removeClass('is-active');

      	setTimeout(function() {
			if ( isBackwards == false || currentPage != 'hub' ) { sections[currentPage].animationFunctionEnter(); }
			//Si hemos terminado la animación de entrada
			setTimeout(function() {
				isAnimating = false;
			}, sections[currentPage].animationTimeEnter);
      	}, 600);

      	//Update history
      	if( !isBackwards ){
        	window.history.pushState({ path: url }, currentPage, url + '#' + language);
      	}

  	});

}

/** Animation callbacks **/

var homeAnimationEnter = function () {
	console.log("home Enter");

	initSliderHome();
	$('header, .footer-main').removeClass('grey');
	$('.layer-mask').removeClass('SlideInUp');

	listenScrollHome();
};

var homeAnimationExit = function () {
	console.log("home Exit");
	
	$('header, .footer-main').toggleClass('grey');
	$('.layer-mask').toggleClass('SlideInUp');

};

var hubAnimationEnter = function () {
	console.log("hub Enter");
	initSliderHub();

	if (isMobile) {
		$('.backimage.mobil-land').css('opacity', '.2');
		$('.image-mask-wrapper').css('opacity', 1);
	}
	$('.controls-hub').removeClass('out');
};

var hubAnimationExit = function () {
	console.log("hub Exit");

	if (isMobile) {
		$('.backimage.mobil-land').css('opacity', 0);
	}
	$('.layer').toggleClass('covered');
	$('.controls-hub').addClass('out');
};

var hubAnimationEnterFromBack = function () {
	console.log('EnterFromBack => transicion de entrada a hub desde landing');

	initSliderHub();

	if (isMobile) {
		$('.backimage.mobil-land').css('opacity', '.2');
		$('.image-mask-wrapper').css('opacity', 1);
	}
	$('.controls-hub').removeClass('out');
};

var hubAnimationExitToBack = function () {
	console.log('ExitToBack => transicion de salida de hub a entrada index');

	$('.container_composition').toggleClass('covered');
	$('header, .footer-main').removeClass('grey');
	$('.controls-hub').addClass('out');
};

var landingAnimationEnter = function () {
	console.log("landing Enter");

	$('.image-mask-layer, .container-mask-land').addClass('scale');
	$('.container-text').addClass('showed');
	$('.backimage').addClass('scale');
	setTimeout(function(){
		$('.cover').removeClass('start');
		$('.footer-landing').removeClass('start');
	}, 900);
	$('header, .footer-main').removeClass('grey').addClass('white');

	coverScrolled = false;
	if( !isMobile ){
		listenScrollLanding();
	}

	$('.social').hide();
};

var landingAnimationExit = function () {
	console.log("landing Exit");

	if (isMobile) {
		$('.backimage.mobil-land').css('opacity', 0);
		setTimeout( function() {
			$('.backimage.mobil-land').removeClass('scale');
			$('.image-mask-layer, .container-mask-land').removeClass('scale');
		}, 600);
	} else {
		$('.backimage').removeClass('scale');
		$('.image-mask-layer, .container-mask-land').removeClass('scale');
	}
	$('.container-text').removeClass('showed');
	$('.footer-landing').addClass('start');
	$('header, .footer-main').removeClass('white').addClass('grey');

	$('.social').show();
};

function initSwiperMenu(){
	//Initialize SwiperMenu
	var swiperMenu = new Swiper('.nav-menu-items-container', {
        initialSlide: 2,
        parallax: true,
        grabCursor: true,
        slidesPerView: 'auto',
        centeredSlides: true,
        paginationClickable: true,
        speed: 900,
        spaceBetween: 25,
		/*  onTouchMoveOpposite: function (swiper, e){
        	console.log(swiper);
        	console.log(swiper.touches.startY + 'start  ');
        	console.log(swiper.touches.currentY + 'current  ');
        }, */
       onSlideChangeStart: function(swiperMenu){
        	/*$('.nav-main-menu-container li').removeClass('high');
        	var navHigh = '#' + swiperMenu.activeIndex;
        	$( navHigh ).addClass('high');*/
        },
        onInit: function(){
        	$('.nav-menu-items-container .swiper-slide-active').removeClass('swiper-slide-active');
        	$('.nav-main-menu-container li').css('opacity', 1);
        }
    });

	//nav-menu slide pagination
	$('.nav-main-menu-container li').on('mouseenter', function(){
        var el = $(this).attr('id');
        swiperMenu.slideTo(el);
    });

	//Add effects on active-slide
    $( '.nav-main-menu-container li' ).hover(
	  function() {
	  	$( '.nav-main-menu-container li').not($( this ) ).css('opacity', 0.3);
	    $( this ).addClass( 'high' );
	    $('.nav-menu-items-container .swiper-slide').not( $('.swiper-slide-active') ).css('opacity', 0.5);
	    $('.nav-menu-items-container .swiper-slide-active').css('opacity', 1);
	  }, function() {
	  	$( '.nav-main-menu-container li').css('opacity', 1);
	    $( this ).removeClass( 'high' );
	  }
	);
	
}

function initSliderHome(){
    var swiperHome = new Swiper('.swiper-home', {
	    speed: 2000,
	    autoplay: 3000,
	    direction: 'horizontal',
	    wrapperClass: 'swiper-home_wrapper',
	    slideClass: 'swiper-home_slide',
	    slideActiveClass: 'swiper-home_slide-active',
	    slideNextClass: 'swiper-home_slide-next',
	    slidePrevClass: 'swiper-home_slide-prev',
	    effect: 'fade'
	});
}

function initSliderHub(){
	 var swiperHub = new Swiper('.swiper-hub', {
		wrapperClass: 'swiper-hub_wrapper',
		slideClass: 'swiper-hub_slide',
		slideActiveClass: 'swiper-hub_slide-active',
		slideNextClass: 'swiper-hub_slide-next',
		slidePrevClass: 'swiper-hub_slide-prev',
		nextButton: '.swiper-hub_button-next',
		prevButton: '.swiper-hub_button-prev',
		initialSlide: currentLandingPosition,
		speed: 100,
		loop: true,
		observer: true,
		observeParents: true,
		onSlideChangeEnd: function(swiper){
			$('.container_composition').removeClass('covered');
		},
		pagination: '.hub-pagination',
		//paginationClickable: true,
		paginationBulletRender: function (swiper, index, className) {
			return '<span class="' + className + '">0' + (index + 1) + '</span>';
		},
		onInit: function(){
			$('.container_composition').removeClass('covered');
			$('.layer').removeClass('covered');
		}
	});
	

	swiperHub.lockSwipes();

		//events actions sliderHub functions
	$('.swiper-hub_button-next').click(function() {
		$('.container_composition').addClass('covered');
		setTimeout(function() {
			swiperHub.unlockSwipes();
			swiperHub.slideNext();
			swiperHub.lockSwipes();
		}, 1100);
	});

	$('.swiper-hub_button-prev').click(function() {
		$('.container_composition').addClass('covered');
		setTimeout(function() {
			swiperHub.unlockSwipes();
			swiperHub.slidePrev();
			swiperHub.lockSwipes();
		}, 1100);
	});

	$('body').on('click', '.swiper-pagination-bullet', function() {
		$('.container_composition').addClass('covered');
		var slideNumber = $(this).html();
		setTimeout(function() {
			swiperHub.unlockSwipes();
			swiperHub.slideTo(slideNumber);
			swiperHub.lockSwipes();
		}, 1100);
	});
}

function listenScrollLanding(){
	$('.block-section').on('scroll', function(e){
		if(!coverScrolled){
			e.preventDefault();
			coverScrolled = true;
			
			scrollCover();

		}
		var st = jQuery(this).scrollTop();
		if(st == 0){
			coverScrolled = false;
		}
	});
}

function scrollCover(){
	var coverPos = $('.content-landing').offset();
	$( '.block-section' ).animate({ scrollTop: coverPos.top - 30 }, 1000, 'swing');

	setTimeout(function() {
		//showContentOnScrolling();
		$('.content-landing').children().animate({'opacity':'1'},500);
	}, 1000); 
}

function showContentOnScrolling(){
	$('.content-landing').children().each( function(i){

        var bottom_of_object = $(this).offset().top + $(this).outerHeight();
        var bottom_of_window = $(window).scrollTop() + $('.content-landing').height();

        /* If the object is completely visible in the window, fade it in */
        if( bottom_of_window > bottom_of_object ){

        	console.log('showing: ' + $(this));
            $(this).stop(true, true).delay(800).animate({'opacity':'1'},500);

        }

    });
}

function checkIsLandingExtension(page){
	if( page != 'index' && page != 'hub' && page != '' ){
		currentLanding = page;

		page = 'landing';
		$('.brand').addClass('landing-transition');

		getPositionSlideFromLanding(currentLanding);
	}else if(page == 'index' || page == ''){
		currentLandingPosition = 0;
		$('.brand').removeClass('landing-transition');
	}

	return page;
}

function getPositionSlideFromLanding(currentLanding){
	currentLandingPosition = landingArray.indexOf(currentLanding);
}

function detectMobil(){
	// device detection
	if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) 
	    || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))){ isMobile = true;
	} else if (window.innerWidth <= '1023') {
		isMobile = true;
	}else{
		isMobile = false;
	}
}

function listenScrollHome(){
	//detect scrolling homme
	var homeContainer = document.getElementById("homepage");
	if (homeContainer.addEventListener) {
	    homeContainer.addEventListener("mousewheel", MouseWheelHandler(), false);
	    homeContainer.addEventListener("DOMMouseScroll", MouseWheelHandler(), false);
	} else {
	    homeContainer.attachEvent("onmousewheel", MouseWheelHandler());
	}

	$(homeContainer).on('touchmove', function() {
        isBackwards = false;

	    newPage = 'hub.html';
	    if( !isAnimating ) changePage(newPage);
	});
}


function MouseWheelHandler() {
    return function (e) {
        // cross-browser wheel delta
        var e = window.event || e;
        var delta = Math.max(-1, Math.min(1, (e.wheelDelta || -e.detail)));

        //scrolling down
        if (delta < 0) {
            console.log('down');
            isBackwards = false;

		    newPage = 'hub.html';
		    if( !isAnimating ) changePage(newPage);
        }

        return false;
    }
}
