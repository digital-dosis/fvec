$(document).ready(function() {
	    
	setHeightProductContainer();    
	$(window).on('resize', function(){
        setHeightProductContainer();
    });

    $(' .product-container .flexslider').flexslider({
        animation: "slide",
        directionNav: false,
        slideshowSpeed: 4000,
        slideshow: false,
        before : function(slider) {
            if (slider.animatingTo != 0) {
                $('.product-graphics').fadeOut('fast');
            } else {
                $('.product-graphics').fadeIn('fast');
            }
        }
    });


    //Popover product-features
    $('.popover-product-feature').popover({
    	container: $('.product-container .row')
    });

    //arrow position popover
    /*$('.popover-product-feature').on('shown.bs.popover', function () {
      $('.popover').css('top',parseInt($('.popover').css('top')) + 22 + 'px')
    })*/

	//Para llenar dinamicamente el contenido de popover-product-feature puedo acceder directamente con la clase: .popover-product-feature
	//$(".popover-product-feature").html('a nice new content');
	
});

function setHeightProductContainer(){
	if( (!isMobile) || ($(window).width() >768) ) {
		$('.product-container, .product-container .container').css('height', $(window).height() + 'px');
	}else{
		$('.product-container, .product-container .container').css('height', 'auto');
	}
}