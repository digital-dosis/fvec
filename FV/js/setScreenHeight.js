$(document).ready(function() {
	    
	setHeightScreenContainer();    
	$(window).on('resize', function(){
        setHeightScreenContainer();
    });

});

function setHeightScreenContainer(){
	if( (!isMobile) || ($(window).width() >768) ) {
		$('.back-blue-page').css('height', ($(window).height() - 60) + 'px');
	}else{
		$('.back-blue-page').css('height', 'auto');
	}
}