$(document).ready(function() {

    //inicializamos el plugin fullpage
	$('#fullpage').fullpage({
		navigation: true,
		responsiveWidth: 769,
		bigSectionsDestination: 'top',
		afterResponsive: function(isResponsive){
		
		},
		onLeave: function(index, nextIndex, direction){
            var leavingSection = $(this);

            //después de abandonar la home
            if(index == 1){
            	// setTime 700
                
                if ( $(window).width() > 768 ) {
                    $('header, #fp-nav').addClass('dark');

                    $('.brand').delay(700)
                        .queue(function (next) { 
                            $(this).css({top: 30, left: 30}); 
                            next();
                    });
                    $('.brand').css('opacity', 0);

                    $('.main-top').hide();
                    
                    setTimeout(function() {
                        $('.main-top').fadeIn('fast');
                    }, 300);
                }
            }

            //antes de dirigirte a la home
            if(nextIndex == 1) {
                if ( $(window).width() > 768 /*!isMobile*/ ) {
                    $('.brand').css('opacity', 0);
                    $('.brand').delay(700)
                        .queue(function (next) { 
                            $(this).css({top: '', left: ''});
                            next(); 
                    });

                    $('header, #fp-nav').removeClass('dark');
                }

            }

            $('.moments').addClass('animate');
            //If scroll is detected resumeCart is hide
            $('.popover-cart').popover('hide');
            
        },
        afterRender:function(){
            var pluginContainer = $(this);
            
            initSlider();
            $('.home .outer').css('height', $(window).height() + 'px');
            btnCompleteOrderPosition();

        },
        afterLoad: function(anchorLink, index){
            var loadedSection = $(this);

            $('.brand').css('opacity', 1);

            if (index == 3) {
                setTimeout(function() {
                    $('.moments').removeClass('animate');
                }, 200);
            }
        },
        afterResize: function(){
            $('.home .outer').css('height', $(window).height() + 'px');
            //$('.section').css('min-height', $(window).height() + 'px');
        }
	});


     $('.flex-caption .btn, .btn-complete-order').on('click', function(){
        $.fn.fullpage.moveSectionDown();
    });


}); //end ready

function initSlider(){
    $('.flexslider').flexslider({
        animation: "slide",
        directionNav: false,
        slideshowSpeed: 4000,
        slideshow: false
    });
}

function btnCompleteOrderPosition(){
    var h = $(window).height();
    var w = $(window).width();
    if(w > 1600){
        var btnPositionH = h - 90;
        var btnPositionW = (w / 2) - 50;
        $('.btn-complete-order').css({
            'position' : 'absolute',
            'top' : btnPositionH + 'px',
            'left' : btnPositionW + 'px'
        });
    }
}

