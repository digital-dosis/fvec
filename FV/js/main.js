var qty = 0;
var isMobile = false;
var isDown = false;
var lastScrollTop = 0;
var actualYear;

$(document).ready(function() {

    Pace.on('done', function() {
        $('#loader').fadeOut(400);

        $('body').addClass("loaded");

        setTimeout(function() {
            $('.brand').addClass("opacity");
        }, 100);
	});

	// device detection
	isMobil();
    checkisMobil();

    $(window).on('resize', function(){
        checkisMobil();
    });

    function animateHeader() {
        $('.scroll-animation').hide();
        setTimeout(function() {
            $('.scroll-animation').fadeIn('fast');
        }, 300);
    };

	//detect scrollY position => change sticky-header  style
	$(window).scroll(function (event) {
	    var scroll = $(window).scrollTop();

        if( scroll >= 45 ){

            // Header animations
            if ( $('.main-header').hasClass('downing') == false) {
                animateHeader();
            }

            // Swap Logo and color
            if ( isMobile ) {
                isDown = true;
                $('.main-header').addClass('downing dark');
            }
            else {
                $('.main-header').addClass('downing');
                $('.header-back-blue').addClass('dark');
            }

        }
        else {

            // Header animations
            if ( $('.main-header').hasClass('downing') == true) {
                animateHeader();
            }

            // Swap Logo and color
            if ( isMobile ) {
                isDown = false;
                if ( $('.hamburger').hasClass('is-active') == false ) {
                    $('.main-header').removeClass('downing dark');
                }
                console.log('nada de downing-movil y el menu está cerrado');
            }
            else {
                $('.main-header').removeClass('downing');
                $('.header-back-blue').removeClass('dark');
            }
        }

        //If scroll is detected resumeCart is hide
        //$('.popover-cart').popover('hide');

        lastScrollTop = scroll;
	});

    //hamburguer menu
    $('.hamburger').on('click', function(){
        $('.hamburger').toggleClass('is-active');
        $('.nav-menu').toggleClass('show');
    });


	// Buy btn hover effect
    $('.btn-buy').not('.btn-buy-mobil').hover(function() {
        $(this).addClass('action');
    },
    function() {
        $(this).removeClass('action');
    });

	//Set info cart resume
	setResumeCartContent();

    //Set info postalcode advise
    setPostalcodeMsgContent();

    //toggle popover
    $('[data-toggle="popover"]').popover();

    if ( !isMobile ) {
        $('body').on('click', function (e) { //Close popover on second click
            if ( $(e.target).hasClass('glyphicon-plus') ||  $(e.target).hasClass('glyphicon-minus') ) {
                return false;
            }

            $('[data-toggle="popover"]').each(function () {
            //the 'is' for buttons that trigger popups
            //the 'has' for icons within a button that triggers a popup
                if (!$(this).is(e.target) &&
                    $(this).has(e.target).length === 0 &&
                    $('.popover').has(e.target).length === 0) {
                        $(this).popover('hide');
                }
            });
        });
    };

    //Solution for bug: Popover needs an extra click when closed programatically
    $('[data-toggle="popover"]').on('hidden.bs.popover', function(e){
        $(e.target).data("bs.popover").inState.click = false;
    });

    //Prevent popover scrolls back to top of the page
    $('.popover-product-feature').on('click', function(e) {e.preventDefault(); return true;});



	//check Postal Code
    $('.glyphicon-plus').on('click', function(){
        $('.qty-cart-btn').html(function(i, val) {
            qty = val*1+1;
            return qty;
        });
        checkPostalCode(qty);
    });


    //fake link login
    $('.container-form-login .btn').on('click', function(e){
        e.preventDefault();
        window.location.replace("http://digitaldosis.com/FV/order");
    });

    //set year copyright
    $('.year').text(getYear());

    //close cookies advise
    $('.close-cookies-advise').on('click', function(){
        $('.cookies-advise').addClass('slideOut');
    });

});


function isMobil(){
	if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent)
    || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) isMobile = true;
}

function checkisMobil(){
    if ($(window).width() <= 1024) { isMobile = true; console.log('mobil');} else { isMobile = false; }
}

function checkPostalCode(qty){
    if(qty == 1){
        return showPostalPopup();
        $('.postalcode-form').show();
        $('.main-top-mode').css('display', 'none');
        $('.nav-categories').addClass('alignleft'); //If postaclCode input is showed align left nav-categories
        showMsgPostalcode();
    }else if( qty == 2 ){
        $('.postalcode-form').hide();
        $('.nav-categories').removeClass('alignleft'); //If postaclCode input is hidden not align left nav-categories
        $('.main-top-cart .cart-resume').show();
        $('.btn-complete-order').addClass('highlight');
        $('.highlight a').html('Completa tu pedido');
        showResumeCart();
    }else{
        return;
    }
}


function showResumeCart(){
    $('.popover-cart').popover('show');
}


function setResumeCartContent(){
    $('.popover-cart').popover({
      container: $('.main-header'),
      placement: 'bottom',
      content: '<div class="row"><form class="form-inline postalcode-form postalcode-form-cart"><div class="form-group"><input type="text" name="postal-code-cart" id="postal-code-cart" class="form-control" placeholder="Introduce tu Código Postal"><span id="submit-postalcode-btn"></span></div></form><div class="col-md-9 popover-cart-product-detail-name"><p>2 x Caja 12 Botellas 1,5l</p><p class="popover-cart-shipping-advise">Envío gratuito a partir de la 3ª caja</p></div><div class="col-md-3 popover-cart-product-detail-price"><span>12,72</span><span>€</span></div></div><div class="row popover-cart-total"><div class="col-md-9 popover-cart-product-total-label">TOTAL</div><div class="col-md-3 popover-cart-product-total-price">17,22€</div></div><div class="btn-checkout-disabled" id="minicartfooterid"><span class="btn btn-go-checkout btn-checkout">Pedido mínimo 15€</span></div>',
      html: true,
      trigger: 'click focus'
      //delay: { "show": 300, "hide": 100 }
    });

    //ng-click="$event.stopPropagation();"

    //Para llenar dinamicamente el contenido de cart puedo acceder directamente con la clase: .popover-content
    //$(".popover-content").html('a nice new content');
    //Tambien podriamos añadirle el contenido mediante el parametro content: "content: $("#popoverContent").html()"

    //Otra opción es crear una plantilla:
    //template: '<div class="row"><div class="col-md-9 popover-cart-product-detail-name"><p></p><p class="popover-cart-shipping-advise"></p></div><div class="col-md-3 popover-cart-product-detail-price"><span></span><span></span></div></div><div class="row popover-cart-total"><div class="col-md-9 popover-cart-product-total-label">TOTAL</div><div class="col-md-3 popover-cart-product-total-price"></div></div><div class="btn btn-white btn-go-checkout"><a href="" target="_blank"></a></div>'
}

function setPostalcodeMsgContent(){
    $('.popover-postalcode').popover({
      container: $('.main-header'),
      placement: 'bottom',
      content: '<div class="row"><p class="advise-postalcode-unavailable-service">Ups! Nuestro servicio aun no distribuye en tu zona pero si quieres puedes dejarnos tu email y te avisaremos cuando esté disponible.</p></div> <div class="row"><form class="form-postalcode-unavailable-service"><input type="text" name="postal-code-email" id="postal-code-email" placeholder="Introduce aquí tu email"/></form></div> <div class="btn btn-white btn-get-service-advise">¡Avisadme!</div>',
      html: true
    });
}

function showMsgPostalcode(){
    $('.popover-postalcode').popover('show');
}

function getYear(){
    var currentTime = new Date();
    var actualYear = currentTime.getFullYear();

    return actualYear;
}
// Postal popup functions
function showPostalPopup() {
  var popup = document.querySelector('.js-postal-popup');
  popup.classList.add('is-visible');
}
function closePostalPopup() {
  var popup = document.querySelector('.js-postal-popup');
  popup.classList.remove('is-visible');
  setTimeout(function () {
    clearPostalPopupError();
  }, 200);
}
function clearPostalPopupInputError() {
  var input = document.querySelector('.js-postal-popup-input');
  input.classList.remove('has-errors');
  input.value = '';
}
function clearPostalPopupError() {
  var text = document.querySelector('.js-postal-popup-text');
  var errorText = document.querySelector('.js-postal-popup-text-error');
  errorText.classList.add('is-hidden');
  text.classList.remove('is-hidden');
  clearPostalPopupInputError();
}
function setPostalPopupError() {
  var text = document.querySelector('.js-postal-popup-text');
  var errorText = document.querySelector('.js-postal-popup-text-error');
  var input = document.querySelector('.js-postal-popup-input');
  input.classList.add('has-errors');
  text.classList.add('is-hidden');
  errorText.classList.remove('is-hidden');
}
function validatePostalPopup() {
  var postalCode = document.querySelector('.js-postal-popup-input').value;
  var error = false;
  clearPostalPopupError();

  // Comprobar que el código postal es correcto, de lo contrario mostrar error
  if (postalCode === '') {
    error = true;
  }

  if (error) {
    setPostalPopupError();
  } else {
    closePostalPopup();
  }
}

// Image popup functions
function showImagePopup() {
  var popup = document.querySelector('.js-image-popup');
  popup.classList.add('is-visible');
}
function closeImagePopup() {
  var popup = document.querySelector('.js-image-popup');
  popup.classList.remove('is-visible');
}
