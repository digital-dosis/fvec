$(document).ready(function() {

	$('#error1').on('click', function(){
		$('.promocode-error-msg .promocode-msg').text('Este código sólo es válido para pedidos a partir de 20€');
		$('#promocodeerrormsg').modal('show');
	});

	$('#error2').on('click', function(){
		$('.promocode-error-msg .promocode-msg').text('El código que has introducido no es válido, prueba a introducirlo de nuevo');
		$('#promocodeerrormsg').modal('show');
	});

	$('#error3').on('click', function(){
		$('.promocode-error-msg .promocode-msg').text('El código que has introducido ya ha sido utilizado, prueba a introducir otro');
		$('#promocodeerrormsg').modal('show');
	});

	$('#error4').on('click', function(){
		$('.promocode-error-msg .promocode-msg').text('El código que has introducido ya ha caducado, prueba a introducirlo otro');
		$('#promocodeerrormsg').modal('show');
	});

	$('#errorminim').on('click', function(){
		$('#qtyminimerrormsg').modal('show');
	});

	$('#errorcredit').on('click', function(){
		$('.promocode-error-msg .promocode-msg').text('¡Has indicado que quieres usar más crédito del que tienes!');
		$('#promocodeerrormsg').modal('show');
	});


	$(function () {
	    $('#datetimepicker').datetimepicker({
	    	format: 'MM/DD/YYYY',
	    	allowInputToggle: true,
	    	locale: 'es'
	    });
	});

	$('.option-need-bill').on( 'change', function() {
	    if( $(this).is(':checked') ) {
			$('.billing').fadeIn();
	    } else {
			$('.billing').hide();
	    }
	});

	$('.option-absent-home').on( 'change', function() {
		var option = $(this).attr('name');
		if( option == 'option-absent-home-one'){
			$( '.option-absent-home-two' ).closest('label').find('span.custom-control-description').addClass('unavailable');
			$( '.info-absent-home-option-two').addClass('unavailable');
			$( '.option-absent-home-one' ).closest('label').find('span.custom-control-description').removeClass('unavailable');
			$( '.info-absent-home-option-one').removeClass('unavailable');
			if( $(this).is(':checked') ) {

				$('.info-block-absent-home-one').fadeIn();

				if( $('.option-absent-home-two').is(':checked') ) {

					$( '.option-absent-home-two' ).prop( "checked", false );
					$('.info-block-absent-home-two').hide();
				}
	    	}else{
	    		$('.info-block-absent-home-one').hide();
	    	}
		}else{
			$( '.option-absent-home-one' ).closest('label').find('span.custom-control-description').addClass('unavailable');
			$( '.info-absent-home-option-one').addClass('unavailable');
			$( '.option-absent-home-two' ).closest('label').find('span.custom-control-description').removeClass('unavailable');
			$( '.info-absent-home-option-two').removeClass('unavailable');
			if( $(this).is(':checked') ) {

				$('.info-block-absent-home-two').fadeIn();
				
				if( $('.option-absent-home-one').is(':checked') ) {
					
					$( '.option-absent-home-one' ).prop( "checked", false ).addClass('unavailable');
					$('.info-block-absent-home-one').hide();
				}
	    	}else{
	    		$('.info-block-absent-home-two').hide();
	    	}
		}
	});
	

	//initial states radiobuttons:

	//compra periódica => radio default is "no" :
	$( '#optionsCompraRecurrenteNo' ).attr('checked','checked');
	$( '#optionsCompraRecurrenteNo' ).prop( "checked", true );

	//payment method  => radio default is "online" :
	$( '#optionsMetodoOnline' ).attr('checked','checked');
	$( '#optionsMetodoOnline' ).prop( "checked", true );

	//save new card checkbox default state => "disabled"
	$( '.new-card-save' ).attr('checked','');
	$('.new-card-save').prop('checked', false);


	//Si la opción compra periódica está seleccionada mostramos el desplegable con las opciones de temporalidad:
	$('input[name^="optionsCompraRecurrente"]').change(function () {
		if (this.value == 'optionSi') {
			$('.recurring-time-selector').css('display', 'inline-block');
		 }else if(this.value == 'optionNo'){
		 	$('.recurring-time-selector').css('display', 'none');
		 }
	});

	//Depende del tipo de pago seleccionado se muestran opciones diferentes:
	$('input[name^="optionsMetodoPago"]').change(function () {
		if (this.value == 'optionOnline') {
			$('.pago-online-fields').css('display', 'block');
			$('.cod-fields').css('display', 'none');
		 }else if(this.value == 'optionCod'){
		 	$('.pago-online-fields').css('display', 'none');
			$('.cod-fields').css('display', 'block');
		 }
	});

	$('.btn-add-card').on('click', function(){
		$('.user-add-new-card').css('display', 'block');
		$('.user-cards-saved').css('display', 'none');
	})
	$('.btn-back-saved-cards').on('click', function(){
		$('.user-cards-saved').css('display', 'block');
		$('.user-add-new-card').css('display', 'none');
	})


	//Popovers info on input fields
	$('.infolabel, .info-member').on('click', function(e) {e.preventDefault()});
	$('.infolabel.infophone').popover({
		placement: 'top',
		content: 'Necesitamos tu número de teléfono para poder ofrecerte un mejor servicio. Contactaremos contigo 30 minutos antes de la entrega. Además de informarte en el caso de que hay alguna incidencia. No te preocupes, no vamos a utilizarlo para ofrecerte promociones ni ofertas, ¡para eso ya tenemos el Newsletter!'
	});

	$('.infolabel.infocomments').popover({
		placement: 'top',
		content: 'Añade algún comentario u observación para el repartidor. Cualquier cosa que creas que puede ser útil para hacer mejor nuestro trabajo, como: “No hay ascensor…”, “El interfono no funciona, podéis dejarle el pedido al portero” o “¡Cuidado que el perro muerde!”.'
	});

	$('.infolabel.infocvv').popover({
		placement: 'top',
		content: 'Introduce el código de verificación. Es el código de tres dígitos que se encuentra en el dorso de tu tarjeta.'
	});

	$('.info-member').popover({
		placement: 'top',
		content: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.'
	});

	$('.info-save-card').popover({
		placement: 'top',
		content: 'No guardamos tu número de tarjeta. La entidad de pago es la única que tiene acceso a la misma, y nos proporciona un código o token para facilitar futuras transacciones.'
	});

	$('.info-absent-home-option-one').popover({
		placement: 'top',
		content: 'Font Vella en Casa no se hace responsable de robos o desperfectos ocasionados tras la entrega.'
	});

	$('.info-absent-home-option-two').popover({
		placement: 'top',
		content: 'Font Vella en Casa no se hace responsable de robos o desperfectos ocasionados tras la entrega.'
	});

	//Show save card radiobutton if new card option is selected
	$('input[type=radio][name=card-payment]').change(function() {
        if (this.value == 'option03') {
           $('.ok-save-card').show();
        }
        else {
            $('.ok-save-card').hide();
        }
    }); 

	//Popovers label title icons order list resume
	$( '.label-title-order-icon' ).hover(
	  function() {
	    $( this ).popover('show');
	  }, function() {
	    $( this ).popover('hide');
	  }
	);



	//jump mode-layout on account: view-edit forms

	//form-datos:
	//=>enter mode edit
	$('.edit-btn-mode').on('click', function(){
		$(this).parent().next('.edit-form').fadeIn();//.css('display', 'block');
		$(this).closest('.container-fields').css('display', 'none');
		//show label change password .
		$('.edit-pw-btn').css('display', 'block');
	});

	//mode change pw .account-password-container
	$('.edit-pw-btn').on('click', function(){
		$('.account-password-container').addClass('edit-pw');
		$('.edit-pw-btn').css('display', 'none');
	});

	//=>back to mode view
	$('.view-btn-mode').on('click', function(){
		$(this).parent().prev().css('display', 'block');
		$(this).parent().css('display', 'none');
		//hide password fields
	});

	//form-address:
	//=>enter mode edit
	$('.btn-add-block-form').on('click', function(){
		$(this).parent().find('.add-block-form').fadeIn();//.css('display', 'block');
		$(this).css('display', 'none');
	});
	//=>back to mode view
	$('.view-btn-mode-add-block-form').on('click', function(){
		$(this).parent().css('display', 'none');
		$(this).parent().next().css('display', 'block');
	});

	//form-cards:
	//=>enter mode edit
	$('.user-cards-saved .field').on('click', function(){
		//$(this).css('display', 'none');
		//$(this).next().fadeIn();//.css('display', 'block');
		$(this).prev().css('display', 'block');
	});
	//=>back to mode view
	$('.view-btn-mode-edit-card').on('click', function(){
		$(this).parent().prev().css('display', 'block');
		$(this).parent().css('display', 'none');
	});

	//add-new-card
	$('.btn-add-card-account').on('click', function(){
		$(this).next().fadeIn();//.css('display', 'block');
		$(this).css('display', 'none');
	});

	//remove card
	$('.edit-btn-remove').on('click', function(){
		$(this).parents('.user-cards-saved').css('display', 'none');
		//$(this).parent().css('display', 'none');
	});

	//back remove card
	$('.edit-btn-back').on('click', function(){
		$(this).parents('.mode-layout').css('display', 'none');
		//$(this).parent().css('display', 'none');
	});

	//los eventos de click de todos los view-btn además de cambiar de vista servirán para enviar los datos del form
	// TO DO: antes de guardar los datos del formulario es necesario validarlos, si alguno de ellos da error no cambiamos al modo vista
	// a los campos inputs que den error de validación les asignaremos la clase: error-validation


	/* Show lost-password-form */
	$('.link-lost-password-form').on('click', function(){
		$('.lost-password').addClass('showing');
	});
	// hide lost-password-form on click dissmis
	$('.dismiss-lost-password').on('click', function(){
		$('.lost-password').removeClass('showing');
	});


	//Automatic copying link member to clipboard
	/*var copyTextareaBtn = document.querySelector('.js-textareacopybtn');

	copyTextareaBtn.addEventListener('click', function(event) {
	  var copyTextarea = document.querySelector('.js-copytextarea');
	  console.log(copyTextarea);
	  copyTextarea.select();

	  try {
	    var successful = document.execCommand('copy');
	    var msg = successful ? 'successful' : 'unsuccessful';
	    console.log('Copying text command was ' + msg);
	  } catch (err) {
	    console.log('Oops, unable to copy');
	  }
	});*/

});