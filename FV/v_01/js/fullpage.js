$(document).ready(function() {

    //inicializamos el plugin fullpage
	$('#fullpage').fullpage({
		navigation: true,
		responsiveWidth: 768,
		bigSectionsDestination: 'top',
		afterResponsive: function(isResponsive){
		
		},
		onLeave: function(index, nextIndex, direction){
            var leavingSection = $(this);

            //después de abandonar la home
            if(index == 1){
            	// setTime 700
                $('header, #fp-nav').addClass('dark');
            }
            else if(index == 2 && direction == 'up'){
            	$('header, #fp-nav').removeClass('dark');	
            }
        },
        afterRender:function(){
            var pluginContainer = $(this);
            
            initSlider();
            $('.home .outer').css('height', $(window).height() + 'px');

        },
        afterLoad: function(anchorLink, index){
            var loadedSection = $(this);

            //usando su índice
            if(index == 1){
                $('header, #fp-nav').removeClass('dark');
            }
        }
	});



}); //end ready

function initSlider(){
    $('.flexslider').flexslider({
        animation: "slide",
        directionNav: false,
        slideshowSpeed: 4000,
        slideshow: false
    });
}

