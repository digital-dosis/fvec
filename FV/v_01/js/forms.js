$(document).ready(function() {

	$('#error1').on('click', function(){
		$('.promocode-error-msg .promocode-msg').text('Este código sólo es válido para pedidos a partir de 20€');
		$('#promocodeerrormsg').modal('show');
	});

	$('#error2').on('click', function(){
		$('.promocode-error-msg .promocode-msg').text('El código que has introducido no es válido, prueba a introducirlo de nuevo');
		$('#promocodeerrormsg').modal('show');
	});

	$('#error3').on('click', function(){
		$('.promocode-error-msg .promocode-msg').text('El código que has introducido ya ha sido utilizado, prueba a introducir otro');
		$('#promocodeerrormsg').modal('show');
	});

	$('#error4').on('click', function(){
		$('.promocode-error-msg .promocode-msg').text('El código que has introducido ya ha caducado, prueba a introducirlo otro');
		$('#promocodeerrormsg').modal('show');
	});

	$('#errorminim').on('click', function(){
		$('#qtyminimerrormsg').modal('show');
	});


	$(function () {
	    $('#datetimepicker').datetimepicker({
	    	format: 'MM/DD/YYYY',
	    	allowInputToggle: true,
	    	locale: 'es'
	    });
	});

	$('.need-bill strong').on('click', function(){
		$('.need-bill').addClass('disabled');
		$('.no-need, .billing').removeClass('disabled');
	});

	$('.no-need strong').on('click', function(){
		$('.need-bill').removeClass('disabled');
		$('.no-need, .billing').addClass('disabled');
	});
	

	//initial states radiobuttons:

	//compra periódica => radio default is "no" :
	$( '#optionsCompraRecurrenteNo' ).attr('checked','checked');
	$( '#optionsCompraRecurrenteNo' ).prop( "checked", true );

	//payment method  => radio default is "online" :
	$( '#optionsMetodoOnline' ).attr('checked','checked');
	$( '#optionsMetodoOnline' ).prop( "checked", true );

	//save new card checkbox default state => "disabled"
	$( '.new-card-save' ).attr('checked','');
	$('.new-card-save').prop('checked', false);


	//Si la opción compra periódica está seleccionada mostramos el desplegable con las opciones de temporalidad:
	$('input[name^="optionsCompraRecurrente"]').change(function () {
		if (this.value == 'optionSi') {
			$('.recurring-time-selector').css('display', 'block');
		 }else if(this.value == 'optionNo'){
		 	$('.recurring-time-selector').css('display', 'none');
		 }
	});

	//Depende del tipo de pago seleccionado se muestran opciones diferentes:
	$('input[name^="optionsMetodoPago"]').change(function () {
		if (this.value == 'optionOnline') {
			$('.pago-online-fields').css('display', 'block');
			$('.cod-fields').css('display', 'none');
		 }else if(this.value == 'optionCod'){
		 	$('.pago-online-fields').css('display', 'none');
			$('.cod-fields').css('display', 'block');
		 }
	});

	$('.btn-add-card').on('click', function(){
		$('.user-add-new-card').css('display', 'block');
		$('.user-cards-saved').css('display', 'none');
	})
	$('.btn-back-saved-cards').on('click', function(){
		$('.user-cards-saved').css('display', 'block');
		$('.user-add-new-card').css('display', 'none');
	})


	//Popovers info on input fields
	$('.infolabel').on('click', function(e) {e.preventDefault()});
	$('.infolabel.infophone').popover({
		placement: 'top',
		content: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis.'
	});

	$('.infolabel.infocomments').popover({
		placement: 'top',
		content: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis.'
	});

	$('.infolabel.infocvv').popover({
		placement: 'top',
		content: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis.'
	});



	//jump mode-layout on account: view-edit forms

	//form-datos:
	//=>enter mode edit
	$('.edit-btn-mode').on('click', function(){
		$(this).parent().next('.edit-form').css('display', 'block');
		$(this).closest('.container-fields').css('display', 'none');
		//show label change password .
		$('.edit-pw-btn').css('display', 'block');
	});

	//mode change pw .account-password-container
	$('.edit-pw-btn').on('click', function(){
		$('.account-password-container').addClass('edit-pw');
		$('.edit-pw-btn').css('display', 'none');
	});

	//=>back to mode view
	$('.view-btn-mode').on('click', function(){
		$(this).parent().prev().css('display', 'block');
		$(this).parent().css('display', 'none');
		//hide password fields
	});

	//form-address:
	//=>enter mode edit
	$('.btn-add-block-form').on('click', function(){
		$(this).parent().find('.add-block-form').css('display', 'block');
		$(this).css('display', 'none');
	});
	//=>back to mode view
	$('.view-btn-mode-add-block-form').on('click', function(){
		$(this).parent().css('display', 'none');
		$(this).parent().next().css('display', 'block');
	});

	//form-cards:
	//=>enter mode edit
	$('.user-cards-saved .field').on('click', function(){
		$(this).css('display', 'none');
		$(this).next().css('display', 'block');
	});
	//=>back to mode view
	$('.view-btn-mode-edit-card').on('click', function(){
		$(this).parent().prev().css('display', 'block');
		$(this).parent().css('display', 'none');
	});

	//add-new-card
	$('.btn-add-card-account').on('click', function(){
		$(this).next().css('display', 'block');
		$(this).css('display', 'none');
	});

	//los eventos de click de todos los view-btn además de cambiar de vista servirán para enviar los datos del form
	// TO DO: antes de guardar los datos del formulario es necesario validarlos, si alguno de ellos da error no cambiamos al modo vista
	// a los campos inputs que den error de validación les asignaremos la clase: error-validation


	/* Show lost-password-form */
	$('.link-lost-password-form').on('click', function(){
		$('.lost-password').addClass('showing');
	});
	// hide lost-password-form on click dissmis
	$('.dismiss-lost-password').on('click', function(){
		$('.lost-password').removeClass('showing');
	});

});