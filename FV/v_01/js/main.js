var qty = 0;
var isMobile = false;

$(document).ready(function() {

	$('.hamburger').on('click', function(){
		$('.hamburger').toggleClass('is-active');
        $('.nav-menu').toggleClass('show');
	});


	// device detection
	isMobil();
    checkisMobil();

    $(window).on('resize', function(){
        checkisMobil();
    });

	//detect scrollY position => change sticky-header  style
	$(window).scroll(function (event) {
	    var scroll = $(window).scrollTop();

	    if( scroll >= 150){
	    	$('.main-header').addClass('downing');
	    	$('.header-back-blue').addClass('dark');
	    }else{
	    	$('.main-header').removeClass('downing');
	    	$('.header-back-blue').removeClass('dark');
	    }

        //If scroll is detected resumeCart is hide
        $('.popover-cart').popover('hide');
	});


	// Buy btn hover effect
    $('.btn-buy').hover(function() {
        $(this).addClass('action');
    },
    function() {
        $(this).removeClass('action');
    });

	//Set info cart resume
	setResumeCartContent();


	//check Postal Code
    $('.glyphicon-plus').on('click', function(){
        $('.qty-cart-btn').html(function(i, val) {
            qty = val*1+1;
            return qty; 
        });
        checkPostalCode(qty);
    });

    //toggleCart
/*    $('.popover-cart').on('click', function(){
        $('.popover-cart').popover('toggle');
    });*/
	
});


function isMobil(){
	if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) 
    || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) isMobile = true;
}

function checkisMobil(){
    if ($(window).width() <= 768) { isMobile = true; console.log('mobil');}
}

function checkPostalCode(qty){
    if(qty == 1){
        $('.postalcode-form').show();
        $('.main-top-mode').css('display', 'none');
        $('.nav-categories').addClass('alignleft'); //If postaclCode input is showed align left nav-categories
    }else if( qty == 2 ){
        $('.postalcode-form').hide(); 
        $('.nav-categories').removeClass('alignleft'); //If postaclCode input is hidden not align left nav-categories
        $('.main-top-cart .cart-resume').show();
        $('.btn-complete-order').addClass('highlight');
        showResumeCart();
    }else{
        return; 
    }
}


function showResumeCart(){
    $('.popover-cart').popover('show');
}


function setResumeCartContent(){
    $('.popover-cart').popover({
      container: $('.main-header'),
      placement: 'bottom',
      content: '<div class="row"><div class="col-md-9 popover-cart-product-detail-name"><p>2 x Caja 12 Botellas 1,5l</p><p class="popover-cart-shipping-advise">Envío gratuito a partir de la 3ª caja</p></div><div class="col-md-3 popover-cart-product-detail-price"><span>12,72</span><span>€</span></div></div><div class="row popover-cart-total"><div class="col-md-9 popover-cart-product-total-label">TOTAL</div><div class="col-md-3 popover-cart-product-total-price">17,22€</div></div><div class="btn btn-white btn-go-checkout"><a>Continuar</a></div>',
      html: true,
      //delay: { "show": 300, "hide": 100 }
    });

    //Para llenar dinamicamente el contenido de cart puedo acceder directamente con la clase: .popover-content
    //$(".popover-content").html('a nice new content');
}