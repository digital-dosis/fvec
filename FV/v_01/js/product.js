$(document).ready(function() {
	    
	setHeightProductContainer();    
	$(window).on('resize', function(){
        setHeightProductContainer();
    });

    $(' .product-container .flexslider').flexslider({
        animation: "slide",
        directionNav: false,
        slideshowSpeed: 4000,
        slideshow: false
    });


    //Popover product-features
    $('.popover-product-feature').popover({
    	container: $('.product-container .row')
    });

	//Para llenar dinamicamente el contenido de popover-product-feature puedo acceder directamente con la clase: .popover-product-feature
	//$(".popover-product-feature").html('a nice new content');
	
});

function setHeightProductContainer(){
	if(!isMobile){
		$('.product-container, .product-container .container').css('height', $(window).height() + 'px');
	}else{
		$('.product-container, .product-container .container').css('height', 'auto');
	}
}